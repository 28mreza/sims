<?php

namespace App\Http\Controllers\Kantin;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class KategoriController extends Controller
{
    public function list()
    {
        $kategori = DB::table('mst_kategori')->where('is_deleted', '0')->orderByDesc('id_kategori')->get();

        return view('kantin.admin.kategori.list', compact('kategori'));
    }

    public function store(Request $request)
    {
        $nama_kategori = $request->nama_kategori;
        $created_at = Carbon::now();
        $updated_at = Carbon::now();

        $data = [
            'nama_kategori' => $nama_kategori,
            'created_at' => $created_at,
            'updated_at' => $updated_at,
        ];

        $simpan = DB::table('mst_kategori')->insert($data);

        if($simpan){
            return Redirect::back()->with(['success' => 'Data Berhasil Disimpan']);
        } else{
            return Redirect::back()->with(['warning' => 'Data Gagal Disimpan']);
        }
    }

    public function edit(Request $request)
    {
        $id_kategori = $request->id_kategori;
        $kategori = DB::table('mst_kategori')->where('id_kategori', $id_kategori)->first();

        return view('kantin.admin.kategori.edit', compact('kategori'));
    }

    public function update($id, Request $request) {
        $nama_kategori = $request->nama_kategori;
        $updated_at = Carbon::now();

        $data = [
            'nama_kategori' => $nama_kategori,
            'updated_at' => $updated_at
        ];

        $update = DB::table('mst_kategori')->where('id_kategori', $id)->update($data);
        if($update)
        {
            return Redirect::back()->with(['success' => 'Data Berhasil Diperbarui']);
        } else {
            return Redirect::back()->with(['warning' => 'Data Gagal Diperbarui']);
        }
    }

    public function delete($id) {
        $isDeleted = 1;
        $data = [
            'is_deleted' => $isDeleted
        ];

        $delete = DB::table('mst_kategori')->where('id_kategori', $id)->update($data);
        if($delete){
            return Redirect::back()->with(['success' => 'Data Berhasil Dihapus']);
        } else{
            return Redirect::back()->with(['warning' => 'Data Gagal Dihapus']);
        }
    }
}
