<?php

use App\Http\Controllers\Kantin\KategoriController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('layouts.app');
});

Auth::routes();

Route::get('/kantin/admin/dashboard', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::prefix('kantin')->group(function () {
    Route::prefix('/admin')->group(function () {
        Route::get('/kategori', [KategoriController::class, 'list'])->name('kantin.kategori');
        Route::post('/kategori/store', [KategoriController::class, 'store'])->name('kantin.kategori.store');
        Route::post('/kategori/edit', [KategoriController::class, 'edit'])->name('kantin.kategori.edit');
        Route::post('/kategori/update/{id}', [KategoriController::class, 'update'])->name('kantin.kategori.update');
        Route::post('/kategori/delete/{id}', [KategoriController::class, 'delete'])->name('kantin.kategori.delete');
    });
});
