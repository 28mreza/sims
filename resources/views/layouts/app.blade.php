<!doctype html>
<html lang="en" data-layout="vertical" data-layout-style="detached" data-sidebar="light" data-topbar="dark"
    data-sidebar-size="lg" data-sidebar-image="none" data-preloader="disable">

@include('partials.kantin.head')

<body>

    <!-- Begin page -->
    <div id="layout-wrapper">

        @include('partials.kantin.navbar')
        
        @include('partials.kantin.sidebar')

        <!-- ============================================================== -->
        <!-- Start right Content here -->
        <!-- ============================================================== -->
        <div class="main-content">

            <div class="page-content">
                <div class="container-fluid">

                    <!-- start page title -->
                    @yield('content')
                    <!-- end page title -->

                </div>
                <!-- container-fluid -->
            </div>
            <!-- End Page-content -->

            @include('partials.kantin.footer')
        </div>
        <!-- end main content-->

    </div>
    <!-- END layout-wrapper -->

    @include('partials.kantin.preloader')

    @include('partials.kantin.themes')

    @include('partials.kantin.script')
    
</body>

</html>