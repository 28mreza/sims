@extends('layouts.app')

@section('content')

<div class="row">
    <div class="col-12">
        <div class="page-title-box d-sm-flex align-items-center justify-content-between">
            <h4 class="mb-sm-0">E-Kantin</h4>

            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Kategori</a></li>
                    <li class="breadcrumb-item active">List</li>
                </ol>
            </div>

        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        @if (Session::get('success'))
        <div class="alert alert-success">
            {{ Session::get('success') }}
        </div>
        @endif
        @if (Session::get('warning'))
        <div class="alert alert-warning">
            {{ Session::get('warning') }}
        </div>
        @endif
    </div>
    <div class="card">
        <div class="card-header">
            <h5 class="card-title mb-0">List Kategori</h5>
        </div>
        <div class="card-body">
            <a href="#" class="btn btn-primary mb-3" id="btn-add">
                <i class="ri-add-line align-bottom me-1"></i>
                Tambah Kategori
            </a>
            <table id="alternative-pagination" class="table nowrap dt-responsive align-middle table-hover table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>ID Kategori</th>
                        <th>Nama Kategori</th>
                        <th>Created At</th>
                        <th>Updated At</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($kategori as $row)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $row->id_kategori }}</td>
                        <td>{{ $row->nama_kategori }}</td>
                        <td>{{ $row->created_at }}</td>
                        <td>{{ $row->updated_at }}</td>
                        <td>
                            <ul class="list-inline hstack gap-2 mb-0">
                                <li class="list-inline-item edit" title="Edit">
                                    <a href="#" class="text-primary d-inline-block btn-edit" id_kategori="{{ $row->id_kategori }}">
                                        <i class="ri-pencil-fill fs-16"></i>
                                    </a>
                                </li>
                                <li class="list-inline-item" data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-placement="top" title="Remove">
                                    <form action="{{ url('kantin/admin/kategori/delete/'.$row->id_kategori) }}" method="POST">
                                        @csrf
                                        <a class="text-danger d-inline-block btn-delete">
                                            <i class="ri-delete-bin-5-fill fs-16"></i>
                                        </a>
                                    </form>
                                </li>
                            </ul>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
</div>

{{-- modal add --}}
<div class="modal fade" id="modal-add" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header bg-light p-3">
                <h5 class="modal-title" id="exampleModalLabel">Tambah Kategori</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close" id="close-modal"></button>
            </div>
            <form action="{{ url('kantin/admin/kategori/store') }}" method="POST" class="tablelist-form" autocomplete="off" id="form-kategori" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">

                    <div class="mb-3">
                        <label for="" class="form-label">Kategori</label>
                        <input type="text" id="nama_kategori" name="nama_kategori" class="form-control" placeholder="Nama Kategori" />
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="hstack gap-2 justify-content-end">
                        <button type="button" class="btn btn-light" data-bs-dismiss="modal">
                            Batal
                        </button>
                        <button type="submit" class="btn btn-primary" id="add-btn">
                            Simpan
                        </button>
                        <!-- <button type="button" class="btn btn-success" id="edit-btn">Update</button> -->
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

{{-- modal edit --}}
<div class="modal fade" id="modal-edit" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header bg-light p-3">
                <h5 class="modal-title" id="exampleModalLabel">Edit Kategori</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close" id="close-modal"></button>
            </div>
            <div class="modal-body" id="load-edit-form"></div>
        </div>
    </div>
</div>
@endsection

@push('myscript')
<script>
    $(function() {
        $("#btn-add").click(function() {
            $("#modal-add").modal("show");
        });

        $(".btn-edit").click(function() {
            var id_kategori = $(this).attr('id_kategori');

            console.log(id_kategori)
            $.ajax({
                type: 'POST'
                , url: '/kantin/admin/kategori/edit'
                , cache: false
                , data: {
                    _token: "{{ csrf_token() }}"
                    , id_kategori: id_kategori
                }
                , success: function(respond) {
                    $("#load-edit-form").html(respond);
                }
            });
            $("#modal-edit").modal("show");
        });

        // form
        $("#form-kategori").submit(function() {
            var nama_kategori = $("#nama_kategori").val();
            if (nama_kategori == "") {
                Swal.fire({
                    title: 'Warning!'
                    , text: 'Nama Kategori Harus Diisi'
                    , icon: 'warning'
                    , confirmButtonText: 'OK'
                });
                $("#nama_kategori").focus();
                return false;
            }
        });

        // delete
        $(".btn-delete").click(function(e){
            var form = $(this).closest('form');
            e.preventDefault();
            Swal.fire({
                title: 'Apakah Anda Yakin Data ini akan Hapus?',
                text: "Jika Ya Maka Data Akan Terhapus Permanen!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya, Hapus Saja!'
                }).then((result) => {
                if (result.isConfirmed) {
                    form.submit();
                    Swal.fire(
                    'Terhapus!',
                    'Data Berhasil Di Hapus.',
                    'success'
                    )
                }
            })
        })
    })

</script>
@endpush
