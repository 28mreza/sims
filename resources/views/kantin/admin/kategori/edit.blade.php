<form action="{{ url('kantin/admin/kategori/update/'.$kategori->id_kategori) }}" method="POST" class="tablelist-form"
autocomplete="off" id="form-kategori" enctype="multipart/form-data">
@csrf
<div class="modal-body">

    <div class="mb-3">
        <label for="" class="form-label">ID Kategori</label>
        <input type="text" id="id_kategori" name="id_kategori" class="form-control" value="{{ $kategori->id_kategori }}" readonly/>
    </div>

    <div class="mb-3">
        <label for="" class="form-label">Kategori</label>
        <input type="text" id="nama_kategori" name="nama_kategori" class="form-control"
            placeholder="Nama Kategori" value="{{ $kategori->nama_kategori }}"/>
    </div>
</div>
<div class="modal-footer">
    <div class="hstack gap-2 justify-content-end">
        <button type="button" class="btn btn-light" data-bs-dismiss="modal">
            Batal
        </button>
        <button type="submit" class="btn btn-primary" id="add-btn">
            Perbarui
        </button>
        <!-- <button type="button" class="btn btn-success" id="edit-btn">Update</button> -->
    </div>
</div>
</form>